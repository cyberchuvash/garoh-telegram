import re
import os
import random

import telebot

TG_TOKEN = os.getenv('GAROH_TG_TOKEN')
tb = telebot.TeleBot(TG_TOKEN)


@tb.message_handler(
    func=lambda m: m.text is not None and re.search(r'г+[ао]+р+о+м?х+', m.text, flags=re.IGNORECASE)
)
def handle_garoh(message: telebot.types.Message):
    tb.reply_to(message, '🤢' * random.randint(1, 5))


@tb.message_handler(
    func=lambda m: m.text is not None and re.search(r'🤢+', m.text, flags=re.IGNORECASE)
)
def handle_emoji(message: telebot.types.Message):
    tb.reply_to(message, f'гар{"о" * random.randint(1, 4)}х')


if __name__ == '__main__':
    tb.infinity_polling(
        allowed_updates=["message", "edited_message"]
    )
