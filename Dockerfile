FROM python:3.11-alpine

COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt

COPY run.py /app/
WORKDIR /app
CMD ["python3", "run.py"]
